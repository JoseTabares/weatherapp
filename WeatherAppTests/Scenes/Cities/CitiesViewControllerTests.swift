//
//  CitiesViewControllerTests.swift
//  WeatherApp
//
//  Created by Jose Efrain Tabares Ramirez on 28/04/22.
//  Copyright (c) 2022 ___ORGANIZATIONNAME___. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

@testable import WeatherApp
import XCTest

class CitiesViewControllerTests: XCTestCase
{
    // MARK: Subject under test
    
    var sut: CitiesViewController!
    var window: UIWindow!
    
    // MARK: Test lifecycle
    
    override func setUp()
    {
        super.setUp()
        window = UIWindow()
        setupCitiesViewController()
    }
    
    override func tearDown()
    {
        window = nil
        super.tearDown()
    }
    
    // MARK: Test setup
    
    func setupCitiesViewController()
    {
        let bundle = Bundle.main
        let storyboard = UIStoryboard(name: "Main", bundle: bundle)
        sut = storyboard.instantiateViewController(withIdentifier: "CitiesViewController") as! CitiesViewController
    }
    
    func loadView()
    {
        window.addSubview(sut.view)
        RunLoop.current.run(until: Date())
    }
    
    // MARK: Test doubles
    
    class CitiesBusinessLogicSpy: CitiesBusinessLogic
    {
        var fetchCitiesCalled = false
        func fetchCities() {
            fetchCitiesCalled = true
        }
        
        var observeCitiesCalled = false
        func observeCities() {
            observeCitiesCalled = true
        }
        
        var fetchWeatherDetailBy = false
        func fetchWeatherDetailBy(lat: Double, lon: Double) {
            fetchWeatherDetailBy = true
        }
    }
    
    // MARK: Tests
    
    func testViewDidLoadShpuldObserveCitiesAndFetchIt()
    {
        // Given
        let spy = CitiesBusinessLogicSpy()
        sut.interactor = spy

        // When
        loadView()

        // Then
        XCTAssertTrue(spy.observeCitiesCalled, "observeCities() should be called")
        XCTAssertTrue(spy.fetchCitiesCalled, "fetchCities() should be called")
    }
    
    func testdisplayAlertShouldPresentAlert()
    {
        // Given
        let spy = CitiesBusinessLogicSpy()
        sut.interactor = spy

        // When
        loadView()
        sut.display(citiesAlert: WeatherError(title: "title", description: "Error", kind: .defaultError, internalError: nil))

        // Then
        XCTAssertTrue(sut.presentedViewController is UIAlertController)
    }
}
