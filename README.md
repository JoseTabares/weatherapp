# Weather App

### Support from iOS 13

Simple weather app using OpenWeather API
Core functionality: the app must be a simple master/detail app, it should include a list of
predefined cities (it’s up to the developer to select these cities) and a current location based by
GPS.
After selecting a city, app must show a detailed weather info for this city (forecast and current
weather).

## Implemented architecture

### Clean Swift – Clean Architecture for iOS

The Clean Swift architecture is derived from the Clean Architecture proposed by Uncle Bob. They share many common concepts such as the components, boundaries, and models.

### The VIP Cycle

The view controller, interactor, and presenter are the three main components of Clean Swift. They act as input and output to one another as shown in the following diagram.

![title](https://clean-swift.com/wp-content/uploads/2015/08/VIP-Cycle.png)

The view controller’s output connects to the interactor’s input. The interactor’s output connects to the presenter’s input. The presenter’s output connects to the view controller’s input. We’ll create special objects to pass data through the boundaries between the components. This allows us to decouple the underlying data models from the components. These special objects consists of only primitive types such as Int, Double, and String. We can create structs, classes, or enums to represent the data but there should only be primitive types inside these containing entities.

## Continuous integration

The .gitlab-ci.yml file has one stage named test, this stage runs unit test and generate junit report and coverage report.