//
//  WeatherView.swift
//  WeatherApp
//
//  Created by Jose Efrain Tabares Ramirez on 27/04/22.
//

import UIKit

@IBDesignable
class WeatherView: UIView {
    
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var weatherImage: UIImageView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupView()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    private func setupView() {
        guard let view = self.loadViewFromNib(nibName: "WeatherView") else { return }
        view.frame = self.bounds
        addSubview(view)
    }
    
    
}

