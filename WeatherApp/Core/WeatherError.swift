//
//  WeatherError.swift
//  WeatherApp
//
//  Created by Jose Efrain Tabares Ramirez on 27/04/22.
//

struct WeatherError: Error {
    enum ErrorKind {
        case defaultError
        case internetConnection
        case unauthorized
        case severError
    }
    
    let title: String
    let description: String
    let kind: ErrorKind
    let internalError: Error?
}



