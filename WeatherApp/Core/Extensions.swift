//
//  Extensions.swift
//  WeatherApp
//
//  Created by Jose Efrain Tabares Ramirez on 27/04/22.
//

import Foundation
import UIKit

extension String {
    func localized() -> String {
        return NSLocalizedString(self, tableName: "Localization", bundle: .main, value: self, comment: self)
    }
}


extension UIViewController {
    
    func display(error: WeatherError, withTrayAgainFunction completion: @escaping () -> Void) {
        let buttonTitle : String
        if error.kind == .internetConnection {
            buttonTitle = L10NConstans.tryAgain
        } else {
            buttonTitle = L10NConstans.ok
        }
        let actions = [UIAlertAction(title: buttonTitle.localized(), style: .default, handler: { action in
            if error.kind == .internetConnection {
                completion()
            }
        })]
        self.presentAlertWith(error: error, actions: actions)
    }
    
    func presentAlertWith(error: WeatherError, actions: [UIAlertAction]) {
        let alert = UIAlertController(title: error.title.localized(), message: error.description.localized(), preferredStyle: .alert)
        for action in actions {
            alert.addAction(action)
        }
        self.present(alert, animated: true, completion: nil)
    }
}

extension UIImageView {
    func downloaded(from url: URL, contentMode mode: ContentMode = .scaleAspectFit) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() { [weak self] in
                self?.image = image
            }
        }.resume()
    }
    func downloaded(from link: String, contentMode mode: ContentMode = .scaleAspectFit) {
        guard let url = URL(string: link) else { return }
        downloaded(from: url, contentMode: mode)
    }
}

extension UIView {
    func apply(cornerRadius: CGFloat) {
        self.clipsToBounds = false
        self.layer.cornerRadius = cornerRadius
    }
    
    func loadViewFromNib(nibName: String) -> UIView? {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first as? UIView
    }
}
