//
//  L10NConstants.swift
//  WeatherApp
//
//  Created by Jose Efrain Tabares Ramirez on 27/04/22.
//

class L10NConstans {
    static let notInternetConnectionTitle = "notInternetConnectionTitle"
    static let notInternetConnection = "notInternetConnection"
    static let weather = "weather"
    static let defultErrorTitle = "defultErrorTitle"
    static let defultError = "defaultError"
    static let unauthorizedTitle = "unauthorizedTitle"
    static let unauthorized = "unauthorized"
    static let ok = "ok"
    static let tryAgain = "tryAgain"
    static let cancel = "cancel"
    static let minimum = "minimum"
    static let maximum = "maximum"
    static let humidity = "humidity"
    static let visibility = "visibility"
    static let pressure = "pressure"
}
