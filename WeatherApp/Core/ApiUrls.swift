//
//  ApiUrls.swift
//  WeatherApp
//
//  Created by Jose Efrain Tabares Ramirez on 27/04/22.
//

enum ApiURLs: String {
    case s3BaseUrl = "https://babiecatest.s3.us-east-2.amazonaws.com"
    case weatherBaseUrl = "http://api.openweathermap.org/data/2.5"
    case weatherIconUrl = "http://openweathermap.org/img/wn/%@@2x.png"
    enum Cities: String {
        case get = "/few-cities.json"
    }
    enum Weather: String {
        case get = "/weather"
        case appId = "0f55e6c12c284a75a0af1787079281f0"
    }
}
