//
//  WeatherDetailWorker.swift
//  WeatherApp
//
//  Created by Jose Efrain Tabares Ramirez on 27/04/22.
//  Copyright (c) 2022 ___ORGANIZATIONNAME___. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

protocol WeatherDetailWorker
{
    func observeWeatherBy(id: Int, completion: @escaping (WeatherDetail.Response?) -> Void)
    func fetchWeatherDetailBy(lat: Double, lon: Double, completion: @escaping (WeatherDetail.Response?, WeatherError?) -> Void)
    func invalidateObserver()
}

class WeatherDetailWorkerAdapter: WeatherDetailWorker {
    
    var apiSource : WeatherApiSource?
    var dbSource : WeatherDBSource?
    
    func observeWeatherBy(id: Int, completion: @escaping (WeatherDetail.Response?) -> Void) {
        dbSource?.observeWeatherBy(id: id, completion: completion)
    }
    
    func fetchWeatherDetailBy(lat: Double, lon: Double, completion: @escaping (WeatherDetail.Response?, WeatherError?) -> Void) {
        apiSource?.getBy(lat: lat, lon: lon) { [weak self] (result, error) in
            if let weatherDetail = result {
                self?.dbSource?.save(weatheDetail: weatherDetail)
                completion(weatherDetail, nil)
                return
            }
            if let error = error {
                completion(nil, error)
            }
        }
    }
    
    func invalidateObserver() {
        dbSource?.invalidateObserver()
    }
}
