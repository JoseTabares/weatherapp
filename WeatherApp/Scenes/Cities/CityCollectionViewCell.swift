//
//  CityCollectionViewCell.swift
//  WeatherApp
//
//  Created by Jose Efrain Tabares Ramirez on 28/04/22.
//

import UIKit

class CityCollectionViewCell: UICollectionViewCell {
    
    // MARK: Identifier
    
    static let identifier = "CityCell"
    
    // MARK: Views
    
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var countryLabel: UILabel!
    
    func loadData(city: Cities.City.ViewModel) {
        cityLabel.text = city.name
        countryLabel.text = city.country
    }
}
