//
//  WeatherApiSource.swift
//  WeatherApp
//
//  Created by Jose Efrain Tabares Ramirez on 27/04/22.
//

import Foundation

protocol WeatherApiSource {
    func getBy(lat: Double, lon: Double, completion: @escaping (WeatherDetail.Response?, WeatherError?) -> Void)
}

class WeatherApiSourceAdapter: ApiSource, WeatherApiSource {
    
    func getBy(lat: Double, lon: Double, completion: @escaping (WeatherDetail.Response?, WeatherError?) -> Void) {
        let langCode = Locale.current.languageCode ?? "en"
        let queryItems = "?lat=\(lat)&lon=\(lon)&appid=\(ApiURLs.Weather.appId.rawValue)&lang=\(langCode)&units=metric"
        let url = URL(string: baseUrl + ApiURLs.Weather.get.rawValue + queryItems)!
        get(url: url, type: WeatherDetail.Response.self, completion: completion)
    }
}

