//
//  CitiesApiSource.swift
//  WeatherApp
//
//  Created by Jose Efrain Tabares Ramirez on 28/04/22.
//

import Foundation

protocol CitiesApiSource {
    func get(completion: @escaping ([Cities.City.Response]?, WeatherError?) -> Void)
}

class CitiesApiSourceAdapter: ApiSource, CitiesApiSource {
    
    func get(completion: @escaping ([Cities.City.Response]?, WeatherError?) -> Void) {
        let url = URL(string: baseUrl + ApiURLs.Cities.get.rawValue)!
        get(url: url, type: [Cities.City.Response].self, completion: completion)
    }
}
