//
//  ApiSource.swift
//  WeatherApp
//
//  Created by Jose Efrain Tabares Ramirez on 27/04/22.
//

import Foundation

internal class ApiSource {
    
    let baseUrl: String
    
    init(baseUrl: String) {
        self.baseUrl = baseUrl
    }
    
    func get<T>(url: URL, type: T.Type, completion: @escaping (T?, WeatherError?) -> Void) where T : Decodable {
        print(url)
        let urlRequest = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalCacheData, timeoutInterval: 30.0)
        let task = URLSession.shared.dataTask(with: urlRequest, completionHandler: { (data, response, error) in
            DispatchQueue.main.async {
                if let error = error {
                    completion(nil, WeatherError(title: L10NConstans.notInternetConnectionTitle, description: L10NConstans.notInternetConnection, kind: .internetConnection, internalError: error))
                    return
                }
                
                if let httpResponse = response as? HTTPURLResponse, !(200...299).contains(httpResponse.statusCode) {
                    var errorKind : WeatherError.ErrorKind
                    var title : String
                    var description : String
                    if [401, 403].contains(httpResponse.statusCode) {
                        errorKind = .unauthorized
                        title = L10NConstans.unauthorizedTitle
                        description = L10NConstans.unauthorized
                    } else {
                        errorKind = .severError
                        title = L10NConstans.defultErrorTitle
                        description = L10NConstans.defultError
                    }
                    completion(nil, WeatherError(title: title, description: description, kind: errorKind, internalError: nil))
                    return
                    
                }
                
                if let data = data, let result = try? JSONDecoder().decode(type, from: data) {
                    completion(result, nil)
                }
            }
        })
        task.resume()
    }
}

