//
//  CitiesDBSource.swift
//  WeatherApp
//
//  Created by Jose Efrain Tabares Ramirez on 28/04/22.
//

import RealmSwift

protocol CitiesDBSource {
    func observeCities(completion: @escaping ([Cities.City.Response]) -> Void)
    func saveAll(cities: [Cities.City.Response])
}

class CitiesDBSourceAdapter: CitiesDBSource {
    
    var localRealm: Realm?
    var notificationToken: NotificationToken?
    
    func observeCities(completion: @escaping ([Cities.City.Response]) -> Void) {
        
        if notificationToken != nil {
            return
        }
        
        let results =  localRealm!.objects(CityDBO.self)
        
        // Retain notificationToken as long as you want to observe
        notificationToken = results.observe { (changes) in
            switch changes {
            case .initial:
                // Results are now populated and can be accessed without blocking the UI
                let cities = results.map { cityDb in  cityDb.toResponse() }
                completion(Array(cities))
                break
            case .update:
                // Query results have changed.
                let cities = results.map { cityDb in  cityDb.toResponse() }
                completion(Array(cities))
            case .error(let error):
                // An error occurred while opening the Realm file on the background worker thread
                fatalError("\(error)")
            }
        }
    }
    
    func saveAll(cities: [Cities.City.Response]) {
        let dbCities = cities.map { city in CityDBO(response: city) }
        try! localRealm!.write {
            localRealm!.add(dbCities, update: .all)
        }
    }
    
}
