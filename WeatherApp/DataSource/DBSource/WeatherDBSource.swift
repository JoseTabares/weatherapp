//
//  WeatherDBSource.swift
//  WeatherApp
//
//  Created by Jose Efrain Tabares Ramirez on 27/04/22.
//

import RealmSwift

protocol WeatherDBSource {
    func observeWeatherBy(id: Int, completion: @escaping (WeatherDetail.Response?) -> Void)
    func save(weatheDetail: WeatherDetail.Response)
    func invalidateObserver()
}

class WeatherDBSourceAdapter: WeatherDBSource {
    
    var localRealm: Realm?
    var notificationToken: NotificationToken?
    
    func observeWeatherBy(id: Int, completion: @escaping (WeatherDetail.Response?) -> Void) {
        if notificationToken != nil {
            return
        }
        
        let object =  localRealm!.object(ofType: WeatherDetailDBO.self, forPrimaryKey: id)
        
        completion(object?.toResponse())
        
        notificationToken = object?.observe { change in
            switch change {
            case .change(let item, _):
                completion((item as? WeatherDetailDBO)?.toResponse())
            case .error(_):
                completion(nil)
            case .deleted:
                completion(nil)
            }
        }
    }
    
    func save(weatheDetail: WeatherDetail.Response) {
        try! localRealm!.write {
            localRealm!.add(WeatherDetailDBO(response: weatheDetail), update: .all)
        }
    }
    
    func invalidateObserver() {
        notificationToken?.invalidate()
    }
}


