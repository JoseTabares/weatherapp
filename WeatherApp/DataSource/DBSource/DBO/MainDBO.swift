//
//  MainDBO.swift
//  WeatherApp
//
//  Created by Jose Efrain Tabares Ramirez on 27/04/22.
//

import RealmSwift

class MainDBO: Object {
    @Persisted var temp: Double = 0
    @Persisted var feelsLike: Double = 0
    @Persisted var tempMin: Double = 0
    @Persisted var tempMax: Double = 0
    @Persisted var pressure: Int = 0
    @Persisted var humidity: Int = 0
    
    convenience init(response: WeatherDetail.Main.Response) {
        self.init()
        temp = response.temp
        feelsLike = response.feelsLike
        tempMin = response.tempMin
        tempMax = response.tempMax
        pressure = response.pressure
        humidity = response.humidity
    }
    
    func toResponse() -> WeatherDetail.Main.Response {
        return WeatherDetail.Main.Response(temp: temp, feelsLike: feelsLike, tempMin: tempMin, tempMax: tempMax, pressure: pressure, humidity: humidity)
    }
}

