//
//  WeatherDBO.swift
//  WeatherApp
//
//  Created by Jose Efrain Tabares Ramirez on 27/04/22.
//

import RealmSwift

class WeatherDBO: Object {
    @Persisted(primaryKey: true) var id: Int = 0
    @Persisted var main: String = ""
    @Persisted var weatherDescription: String = ""
    @Persisted var icon: String = ""
    
    convenience init(response: WeatherDetail.Weather.Response) {
        self.init()
        id = response.id
        main = response.main
        weatherDescription = response.description
        icon = response.icon
    }
    
    func toResponse() -> WeatherDetail.Weather.Response {
        return WeatherDetail.Weather.Response(id: id, main: main, description: weatherDescription, icon: icon)
    }
}

