//
//  WeatherDetailDBO.swift
//  WeatherApp
//
//  Created by Jose Efrain Tabares Ramirez on 27/04/22.
//

import RealmSwift

class WeatherDetailDBO: Object {
    @Persisted(primaryKey: true) var id: Int = 0
    @Persisted var name: String = ""
    @Persisted var cod: Int = 0
    @Persisted var coord: CoordDBO?
    @Persisted var weather: List<WeatherDBO>
    @Persisted var main: MainDBO?
    @Persisted var visibility: Int = 0
    
    convenience init(response: WeatherDetail.Response) {
        self.init()
        id = response.id
        name = response.name
        cod = response.cod
        coord = CoordDBO(response: response.coord)
        let weatherList = response.weather.map({ weather in WeatherDBO(response: weather) })
        weather.append(objectsIn: weatherList)
        main = MainDBO(response: response.main)
        visibility = response.visibility
    }
    
    func toResponse() -> WeatherDetail.Response {
        let weatherList = weather.map({ weather in weather.toResponse() })
        return WeatherDetail.Response(id: id, name: name, cod: cod, coord: coord!.toResponse(), weather: Array(weatherList), main: main!.toResponse(), visibility: visibility)
    }
}

