//
//  CityDBO.swift
//  WeatherApp
//
//  Created by Jose Efrain Tabares Ramirez on 27/04/22.
//

import RealmSwift

class CityDBO: Object {
    @Persisted(primaryKey: true) var id: Int = 0
    @Persisted var name: String = ""
    @Persisted var state: String = ""
    @Persisted var country: String = ""
    @Persisted var coord: CoordDBO?
    
    convenience init(response: Cities.City.Response) {
        self.init()
        id = response.id
        name = response.name
        state = response.state
        country = response.country
        coord = CoordDBO(response: response.coord)
    }
    
    func toResponse() -> Cities.City.Response {
        return Cities.City.Response(id: id, name: name, state: state, country: country, coord: coord!.toResponse())
    }
}
