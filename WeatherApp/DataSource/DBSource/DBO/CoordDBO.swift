//
//  CoordDBO.swift
//  WeatherApp
//
//  Created by Jose Efrain Tabares Ramirez on 27/04/22.
//

import RealmSwift

class CoordDBO: Object {
    @Persisted var lon: Double = 0.0
    @Persisted var lat: Double = 0.0
    
    convenience init(response: Cities.Coord.Response) {
        self.init()
        lon = response.lon
        lat = response.lat
    }
    
    func toResponse() -> Cities.Coord.Response {
        return Cities.Coord.Response(lon: lon, lat: lat)
    }
}

